/// @description 
if (hp <= 0) {
	if (!dead) {
		dead = true;
		alarm[1] = 1;
	}
}
if (dead) {
	image_speed = 0;
	if (y > 0 && phy_speed_y > -1) {	
		physics_apply_force(phy_com_x,phy_com_y,0,-global.grav*phy_mass);
	}
	var target_rotation = round(phy_rotation/180)*180;
	phy_angular_velocity = rlerp(phy_angular_velocity,angle_difference(target_rotation,phy_rotation),0.05);
	phy_angular_velocity *= 0.75;
	if (abs(angle_difference(0,phy_rotation)) < 90) {
		image_yscale = -1;
	} else {
		image_yscale = 1;
	}
}
if (jumping_to_boat) {
	phy_active = false;
	jump_timer++;
	var l = jump_timer/jump_time;
	x = lerp(jump_start_x,oFishingBoat.x+jump_end_x,l);
	y = lerp(jump_start_y,oFishingBoat.y,l)-100*(4*l-4*sqr(l));
	
	phy_position_x = x;
	phy_position_y = y;
	if (l > 0.5) {
		depth = oFishingBoat.depth+10;
	}
	if (jump_timer == jump_time) {
		alarm[0] = 1;
	}
} else if (y < 0) {
	physics_apply_force(phy_com_x,phy_com_y,0,global.grav*phy_mass);
} else if (dead || caught) {
	if (distance_to_object(oFishingBoat) < 32) {
		if (!jumping_to_boat) {
			jumping_to_boat = true;
			jump_start_x = x;
			jump_end_x = random_range(-10,10);
			jump_start_y = y;
		}
	}
	if (!caught) {
		phy_speed_x *= 0.95;
		phy_speed_y *= 0.95; 
	}
	image_speed = 0;
}  else {

	mvdir = point_direction(0,0,phy_speed_x,phy_speed_y);
	mvspd = point_distance(0,0,phy_speed_x,phy_speed_y);
	
	var target_mvdir = mvdir;
	var target_mvspd = swim_speed;
	
	
	
		
	var nearby_fish = ds_list_create();
	collision_circle_list(x,y,visual_range,oPlayer,false,true,nearby_fish,false);
	collision_circle_list(x,y,visual_range,oFish,false,true,nearby_fish,false);
	if (flocking) {
		var sum_x = 0;
		var sum_y = 0;
		var sum_spd_x = 0;
		var sum_spd_y = 0;
		var separation_x = 0;
		var separation_y = 0;
		
		var num_fish = 0
		for (var i = 0; i < ds_list_size(nearby_fish); i++;) {
			var f = nearby_fish[|i];
			if (f.object_index != object_index || f.caught || f.dead) {
				continue;
			}
			num_fish++;
			sum_x += f.x;
			sum_y += f.y;
			sum_spd_x += f.phy_speed_x;
			sum_spd_y += f.phy_speed_y;
			if (point_distance(x,y,f.x,f.y) < min_flocking_distance) {
				separation_x += map_range(abs(x-f.x),0,min_flocking_distance,1,0)*sign(x-f.x);
				separation_y += map_range(abs(y-f.y),0,min_flocking_distance,1,0)*sign(y-f.y);
			}
		}
		if (num_fish > 0) {
			var avg_x = sum_x/num_fish;
			var avg_y = sum_y/num_fish;
			var center_dir = point_direction(x,y,avg_x,avg_y);
			var avg_spd = min(swim_speed, point_distance(0,0,sum_spd_x/num_fish,sum_spd_y/num_fish));
			var avg_dir = point_direction(0,0,sum_spd_x/num_fish,sum_spd_y/num_fish);
		
			target_mvdir = rlerp(target_mvdir,center_dir,coherence);
			target_mvspd = lerp(target_mvspd,avg_spd,alignment);
			target_mvdir = rlerp(target_mvdir,avg_dir,alignment);
		
			physics_apply_force(phy_com_x,phy_com_y,separation_x*separation,separation_y*separation);
		}
	}
	
	
	if (aggressive) {
		//chase smaller fish or submarine
		if (target == noone) {
			for (var i = 0; i < ds_list_size(nearby_fish); i++;) {
				if ((f.object_index == oPlayer) || (f.size < size && f.object_index != object_index)) {
					target = f;
					break;
				}
			}
		} else {
			charge_timer++;
			var attack = false;
			if (charge_timer < 0.3*charge_time) {
				target_mvspd = swim_speed/2;
			} else if (charge_timer > 0.7*charge_time){
				target_mvspd = 0;
			} else {
				target_mvspd = charge_speed;
				attack = true;
			}
			with (target) {
				target_mvdir = point_direction(other.x,other.y,x+phy_speed_x*10,y+phy_speed_y*10);
				if (attack && place_meeting(x-lengthdir_x(3,target_mvdir),y-lengthdir_y(3,target_mvdir),other.id)) {
					hp -= other.damage;
					alarm[1] = true;
					if (hp <= 0) {
						alarm[2] = true;
						hp = 0;
					}
					other.charge_timer = 0.75*other.charge_time; // finish the attack
				}
			}
			
			if (charge_timer >= charge_time) {
				charge_timer = 0;
				target = noone;
			}
		}
	}
	// avoid submarine and bigger fish
	if (cowardly) {
		var sum_x = 0;
		var sum_y = 0;
		
		var num_fish = 0;
		for (var i = 0; i < ds_list_size(nearby_fish); i++;) {
			var f = nearby_fish[|i];
			if (f.object_index == oPlayer) {
				if (aggressive) {
					continue;
				}
			}
			if ((f.size > size) && f.aggressive && (f.object_index != object_index) && !f.dead) {
				sum_x += f.x;
				sum_y += f.y;
				num_fish++;
			}
		}
		
		if (num_fish > 0) {
			var avg_x = sum_x/num_fish;
			var avg_y = sum_y/num_fish;
			var threat_dir = point_direction(x,y,avg_x,avg_y);
			target_mvdir += sign(angle_difference(target_mvdir,threat_dir))*45;
			target_mvspd = charge_speed;
		}
	}
	
	
	ds_list_destroy(nearby_fish);
	
	if (y < min_depth) {
		target_mvdir = rclamp(target_mvdir,200,340);
	}
	
	if (y > max_depth) {
		target_mvdir = rclamp(target_mvdir,20,160);
	}


	var target_speed_x = lengthdir_x(target_mvspd,target_mvdir);
	var target_speed_y = lengthdir_y(target_mvspd,target_mvdir);

	/*var acc_x = (target_speed_x-phy_speed_x)*phy_mass;
	var acc_y = (target_speed_y-phy_speed_y)*phy_mass;

	var acc_dir = point_direction(0,0,acc_x,acc_y);
	var acc_force = clamp(point_distance(0,0,acc_x,acc_y),-swim_force,swim_force);
	physics_apply_force(phy_com_x,phy_com_y,lengthdir_x(acc_force,acc_dir),lengthdir_y(acc_force,acc_dir));
	*/
	phy_speed_x = lerp(phy_speed_x,target_speed_x,acceleration);
	phy_speed_y = lerp(phy_speed_y,target_speed_y,acceleration);
	
	image_angle = rlerp(image_angle,rlerp(mvdir,target_mvdir,0.05),0.2);
	phy_rotation = -image_angle;
	var flip =(abs(angle_difference(0,image_angle)) > 90)
	&& (abs(angle_difference(90,image_angle)) > 15)
	&& (abs(angle_difference(270,image_angle)) > 15);
	if (flip != (image_yscale < 0)) {
		image_yscale *= -1;
	}
	image_speed = target_mvspd/swim_speed;
	
}

if (distance_to_object(oPlayer)> (global.radar_range+100)) {
	instance_destroy();
}
/*
var mvdir = point_direction(0,0,hm,vm);
var mvmag = point_distance(0,0,hm,vm);
if (mvmag > 0.5) {
	if (sign(hm) != 0 && abs(angle_difference(mvdir,90)) > 30 && abs(angle_difference(mvdir,270)) > 30) {
		image_index 

	}
	if (image_xscale > 0) {
		image_angle = rlerp(image_angle,mvdir,0.05);	
	} else {
		image_angle = rlerp(image_angle,mvdir+180,0.05);
	}
} else {
	image_angle = rlerp(image_angle,0,0.01);
}
