/// @description 
caught = false;
dead = false;
var size_change = random_range(1-size_variation,1+size_variation);
size *= size_change;
image_xscale *= size_change;
image_yscale *= size_change;

mvdir = 0;
mvspd = 0;

jumping_to_boat = false;
jump_start_x = 0;
jump_start_y = 0;
jump_end_x = 0;
jump_timer = 0;
jump_time = 60;

target = noone;
charge_timer = 0;
charge_time = 180;