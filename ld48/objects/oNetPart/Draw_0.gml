/// @description
var i = 0;
draw_set_color(c_gray);
while (i < ds_list_size(neighbours)) {
	var n = (neighbours[| i]);
	with (n) {
		draw_line(x,y,other.x,other.y);
	}
	i++;
}
if (tethered) {
	draw_line(x,y,oPlayer.x+xoff(0,8,oPlayer.image_angle),oPlayer.y+yoff(0,8,oPlayer.image_angle));
}
draw_set_color(c_white);
draw_self();