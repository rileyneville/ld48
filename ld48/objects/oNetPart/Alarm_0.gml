/// @description break
instance_destroy();
with(fish) {
	caught = false;
}
part_particles_create(global.ps_effects,x,y,global.pt_bubbles,4);