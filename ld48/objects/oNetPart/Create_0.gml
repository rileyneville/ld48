/// @description 
neighbours = ds_list_create();
repulsive_force = 0.25;
max_fish_size = 4;
weighted = false;
tethered = false;
fish = noone;