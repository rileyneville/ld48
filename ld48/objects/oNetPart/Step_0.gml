/// @description 
var i = 0;
while (i < ds_list_size(neighbours)) {
	var n = (neighbours[| i]);
	if (instance_exists(n)) {
		with (n) {
			var dir = point_direction(other.x,other.y,x,y);
			physics_apply_force(phy_com_x,phy_com_y,lengthdir_x(other.repulsive_force,dir),lengthdir_y(other.repulsive_force,dir));
		}
		i++;
	} else {
		ds_list_delete(neighbours,i);
	}
}
if (weighted) {
	physics_apply_force(phy_com_x,phy_com_y,0,0.5);
}
if (instance_exists(fish)) {
	fish.caught = true;
} else {
	fish = noone;
	var new_fish = instance_place(x,y,oFish);
	if (instance_exists(new_fish)) {
		if (new_fish.size <= max_fish_size || new_fish.dead) {
			fish = new_fish;
			physics_joint_revolute_create(id,fish,phy_com_x,phy_com_y,0,0,true,0,0,0,0);
			fish.caught = true;
		} else {
			alarm[0] = 1;
		}
	}
}