/// @description 
if (other.size <= max_fish_size) {
	if (fish == noone) {
		physics_joint_revolute_create(id,other.id,phy_com_x,phy_com_y,0,0,true,0,0,0,0);
		other.caught = true;
		fish = other.id;
	}
} else {
	alarm[0] = 1;
}