/// @description 

var blend = merge_colour(c_black,c_white,map_range(y,1000,6000,0,1,true));
gpu_set_tex_filter(true);
gpu_set_blendmode(bm_subtract);
	var light_dir = image_angle;
	if (image_xscale == -1) {
		light_dir += 180;
	}
	if (sprite_index == sSubmarineTurn) {
		light_dir += 180*image_index/image_number*-image_xscale;
	}
	draw_sprite_ext(sLight,has_light,x,y,4,4,light_dir,blend,1);

gpu_set_tex_filter(false);

gpu_set_blendmode(bm_add);
with (oFish) {
	if (sprite_exists(glow_sprite)) {
		draw_sprite_ext(glow_sprite,image_index,x,y,image_xscale,image_yscale,image_angle,blend,image_alpha);
	}
}
gpu_set_blendmode(bm_normal);