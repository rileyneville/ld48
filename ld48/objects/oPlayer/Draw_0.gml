/// @description 
if (hp <= 0) {
	if (sprite_index == sSubmarine) {
		draw_sprite_ext(sSubmarineHurt,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
	} else if (sprite_index == sSubmarineTurn) {
		draw_sprite_ext(sSubmarineTurnHurt,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
	}
} else {
	draw_self();
}