/// @description 
var last_radar_angle = radar_angle;
radar_angle = (radar_angle - radar_speed) mod 360;
var radar_r = 40;
var radar_x = display_get_gui_width()/2;
var radar_y = display_get_gui_height() - radar_r - 20;
draw_set_alpha(0.25);
draw_set_color(c_black);
draw_circle(radar_x,radar_y,radar_r,false);
draw_set_alpha(0.75);
draw_set_color(c_white);
draw_line_width(radar_x,radar_y,radar_x+lengthdir_x(radar_r,radar_angle),radar_y+lengthdir_y(radar_r,radar_angle),1);

with (oPingable) {
	if (point_distance(x,y,oPlayer.x,oPlayer.y) <= oPlayer.radar_range) {
		var a = point_direction(oPlayer.x,oPlayer.y,x,y);
		var a1 = angle_difference(a,last_radar_angle);
		var a2 = angle_difference(a,other.radar_angle);
		if (abs(a1) < 90 && (sign(a1) != sign(a2))) {
			ping = 1;
		}
	}
	if (ping > 0.1) {
		var dist = (point_distance(x,y,oPlayer.x,oPlayer.y)/oPlayer.radar_range)*radar_r;
		var dir = point_direction(oPlayer.x,oPlayer.y,x,y)
		draw_circle_color(radar_x+lengthdir_x(dist,dir),radar_y+lengthdir_y(dist,dir),size*ping,ping_colour,ping_colour,false);
	}
}
draw_set_halign(fa_center);
draw_set_valign(fa_top);

draw_text(radar_x,radar_y+radar_r,string(floor(y/10))+"m");

draw_set_valign(fa_bottom);
display_cash = lerp(display_cash,cash,0.1);
if (abs(display_cash-cash) < 0.25) {
	display_cash = cash;
}
draw_text(radar_x,radar_y-radar_r,"$"+string(floor(display_cash)));

draw_sprite(sArmour,0,radar_x - radar_r, radar_y + radar_r);
draw_roundrect(radar_x-radar_r-12,lerp(radar_y+radar_r-4,radar_y-radar_r+4,hp/max_hp),radar_x-radar_r-4,radar_y+radar_r-4,false);

draw_sprite(sOxygen,0,radar_x + radar_r, radar_y + radar_r);
draw_roundrect(radar_x+radar_r+4,lerp(radar_y+radar_r-4,radar_y-radar_r+4,oxygen/max_oxygen),radar_x+radar_r+12,radar_y+radar_r-4,false);
draw_set_alpha(1);
draw_set_color(c_white);

draw_set_valign(fa_bottom);
var dy = display_get_gui_height() - 28;
var dx = radar_x + radar_r + 32;
draw_sprite(sNetIcon,1,dx,dy);

if (has_harpoon) {
	dx += 32;
	draw_sprite(sHarpoonIcon,1,dx,dy);
}
if (dynamite > 0) {
	dx += 32;
	draw_sprite(sDynamiteIcon,1,dx,dy);
	draw_text(dx,dy,string(dynamite));
}

if (blackout > 0) {
	draw_set_alpha(map_range(blackout,0,max_blackout-10,0,1,true));
	draw_set_color(c_black);
		draw_rectangle(0,0,display_get_gui_width(),display_get_gui_height(),false);
	draw_set_color(c_white);
	draw_set_alpha(1);
}
