/// @description respawn
oxygen = max_oxygen;
phy_position_y = 0;
phy_position_x = oFishingBoat.x;
phy_speed_y = 0;
phy_speed_x = 0;
cash = 0;

if (net_attached) {
	with (oNetPart) {
		alarm[0] = 1;
	}
	ds_grid_clear(net_parts,noone);
	net_attached = false;
}
if (instance_exists(harpoon)) {
	harpoon.alarm[0] = 1;
	harpoon = noone;
	harpoon_retract = false;
}