/// @description 
var hm = keyboard_check(ord("D"))-keyboard_check(ord("A"));
	hm += keyboard_check(vk_right)-keyboard_check(vk_left);
	hm += gamepad_axis_value(0,gp_axislh);
	hm = clamp(hm,-1,1);
var vm = keyboard_check(ord("S"))-keyboard_check(ord("W"));
	vm += keyboard_check(vk_down)-keyboard_check(vk_up);
	vm += gamepad_axis_value(0,gp_axislv);
	vm = clamp(vm,-1,1);
	
var net_pressed = keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(0,gp_face1);
var harpoon_pressed = keyboard_check_pressed(vk_control) || gamepad_button_check_pressed(0,gp_face3);
var dynamite_pressed = keyboard_check_pressed(vk_shift) || gamepad_button_check_pressed(0,gp_face4);

if (y < 0) {
	vm = map_range(y,0,-10,vm,1,true);
	//hm = 0;
} 

if (bbox_top > 0) {
	var leak_rate = 1;
	if (hp <= 0) {
		leak_rate = broken_leak_rate;	
	}
	oxygen = max(0,oxygen-leak_rate);
} else {
	oxygen = min(max_oxygen,oxygen+50);
}
if (oxygen <= 0) {
	blackout += 1;
	if (blackout >= max_blackout) { // respawn
		if (alarm[0] <= 0) {
			alarm[0] = 1;
		}
	}
} else {
	blackout = max(0,blackout - 3);
}

var mvdir = point_direction(0,0,hm,vm);
var mvmag = point_distance(0,0,hm,vm);
if (mvmag > 0.5) {
	if (sign(hm) != 0 && abs(angle_difference(mvdir,90)) > 30 && abs(angle_difference(mvdir,270)) > 30) {
		if (sprite_index == sSubmarine && sign(image_xscale) != sign(hm)) {
			sprite_index = sSubmarineTurn;
			image_index = 0;
		}
	}
	if (image_xscale > 0) {
		image_angle = rlerp(image_angle,mvdir,0.05);	
	} else {
		image_angle = rlerp(image_angle,mvdir+180,0.05);
	}
} else {
	image_angle = rlerp(image_angle,0,0.05);
}
if (mvmag < 0.1) {
	physics_apply_force(phy_com_x,phy_com_y,0,-0.1*global.grav*phy_mass);
}

phy_rotation = -image_angle;


if (sprite_index == sSubmarineTurn) {
	image_speed = 1;
	var xo = map_range(image_index,0,image_number,-20,20)*image_xscale;
} else {	
	image_speed = lerp(image_speed,abs(hm)+abs(vm),0.1);
	var xo = -20*image_xscale;
}

var px = x+xoff(xo,5,image_angle);
var py = y+yoff(xo,5,image_angle);
if (py > 0 && random(1) < image_speed) {
	part_particles_create(global.ps_effects,px,py,global.pt_bubbles,1);
}

if (y >= 0) {
	physics_apply_force(phy_com_x,phy_com_y,hm*move_force,vm*move_force);
} else {
	physics_apply_force(phy_com_x,phy_com_y,0,global.grav*phy_mass);
}

if (net_pressed) {
	if (net_attached) {
		with (oNetPart) {
			alarm[0] = 1;
		}
		ds_grid_clear(net_parts,noone);
		net_attached = false;
	} else {
		for (var xx = 0; xx < 4; xx++;) {
			for (var yy = 0; yy < 4; yy++;) {
				var n1 = instance_create_depth(x+xoff(0,4)+xx,y+yoff(0,4)+yy,depth+1,oNetPart);
			
				net_parts[# xx,yy] = n1;
				if (xx > 0) {
					var n2 = net_parts[# xx-1,yy];
					ds_list_add(n2.neighbours,n1);
					ds_list_add(n1.neighbours,n2);
					physics_joint_rope_create(n1,n2,n1.phy_com_x,n1.phy_com_y,n2.phy_com_x,n2.phy_com_y,12,false);
				}
				if (yy > 0) {
					var n2 = net_parts[# xx,yy-1];
					ds_list_add(n2.neighbours,n1);
					ds_list_add(n1.neighbours,n2);
					physics_joint_rope_create(n1,n2,n1.phy_com_x,n1.phy_com_y,n2.phy_com_x,n2.phy_com_y,12,false);
				}
				n1.phy_speed_y = 1;
			}
		}
		n1.weighted = true;
		net_parts[# 0,0].tethered = true;
		physics_joint_rope_create(net_parts[# 0,0],id,x,y,x,y,20,false);
		net_attached = true;
	}
}

if (harpoon_retract) {
	var x1 = harpoon.x;
	var y1 = harpoon.y;
	var x2 = x+xoff(0,8);
	var y2 = y+yoff(0,8);
	var dir = point_direction(x1,y1,x2,y2);
	var pull_force = 50;
	var fx = lengthdir_x(pull_force,dir);
	var fy = lengthdir_y(pull_force,dir);
	with (harpoon) {
		physics_apply_force(x,y,fx,fy);
	}
	physics_apply_force(x+xoff(0,8),y+yoff(0,8),-fx,-fy);
	
	if (distance_to_object(harpoon) <= 4) {	
		harpoon.alarm[0] = 1;
		harpoon = noone;
		harpoon_retract = false;
	}
}
if (harpoon_pressed && has_harpoon) {
	if (instance_exists(harpoon)) {
		harpoon_retract = true;	
	} else {
		harpoon = instance_create_depth(x+xoff(0,8),y+yoff(0,8),depth+1,oHarpoon);
		part_particles_create(global.ps_effects,x+xoff(0,8),y+yoff(0,8),global.pt_bubbles,15);
		var hdir = image_angle;
		if (image_xscale < 0) {
			hdir += 180;
		}
		harpoon.image_angle = hdir;
		harpoon.phy_rotation = -hdir;
		harpoon.phy_speed_x = phy_speed_x + lengthdir_x(6,hdir);
		harpoon.phy_speed_y = phy_speed_y + lengthdir_y(6,hdir);
		physics_apply_impulse(phy_com_x,phy_com_y,-lengthdir_x(6,hdir)*harpoon.phy_mass,-lengthdir_y(6,hdir)*harpoon.phy_mass);
		var x1 = harpoon.x;
		var y1 = harpoon.y;
		var x2 = x+xoff(0,8);
		var y2 = y+yoff(0,8);
		harpoon.max_distance = max_harpoon_distance;
	}
}

if (dynamite_pressed) {
	if (dynamite > 0) {
		dynamite--;
		var d = instance_create_layer(x,y,"Fish",oDynamite);
		d.phy_speed_y = phy_speed_y + 0.5;
		d.phy_speed_x = phy_speed_x;
		d.phy_angular_velocity = random_range(-90,90);
	}
	
}