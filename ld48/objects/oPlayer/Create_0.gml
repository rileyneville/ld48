/// @description 
gamepad_set_axis_deadzone(0,0.1);



/// stats

move_force = 25;
has_light = false;
has_harpoon = false;
dynamite = 0;
radar_range = 500;
max_oxygen = 1200;
oxygen = max_oxygen;
max_hp = 10;
hp = max_hp;


broken_leak_rate = 20;
cash = 0;
display_cash = 0;
dead = false;

net_attached = false;
net_parts = ds_grid_create(4,4);
ds_grid_clear(net_parts,noone);

harpoon = noone;
harpoon_retract = false;
max_harpoon_distance = 200;

size = 10;
aggressive = true;
radar_angle = -90;
radar_speed = 3;

blackout = 0;
max_blackout = 180;
