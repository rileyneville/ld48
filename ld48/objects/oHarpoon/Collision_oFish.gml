/// @description 
if (fish == noone) {
	physics_joint_revolute_create(id,other.id,phy_com_x,phy_com_y,0,0,true,0,0,0,0);
	other.hp -= damage;
	fish = other.id;
	fish.alarm[1] = 1;
	var x1 = x;
	var y1 = y;
	var x2 = oPlayer.x+xoff(0,8,oPlayer.image_angle)
	var y2 = oPlayer.y+yoff(0,8,oPlayer.image_angle)
	var dist = point_distance(x1,y1,x2,y2);
	physics_joint_rope_create(id,oPlayer,x1,y1,x2,y2,dist,false);
	oPlayer.harpoon_retract = false;
}
