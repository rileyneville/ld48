/// @description 
if (y< 0) {
	physics_apply_force(phy_com_x,phy_com_y,0,global.grav*phy_mass);
}
if (fish = noone && !oPlayer.harpoon_retract) {
	physics_apply_force(phy_com_x,phy_com_y,0,(global.grav/4)*phy_mass);
	image_angle = rlerp(image_angle,point_direction(0,0,phy_speed_x,phy_speed_y),0.2);
	phy_rotation = -image_angle;
}
if (!instance_exists(fish) && distance_to_object(oPlayer) > max_distance) {
	oPlayer.harpoon_retract = true;
}