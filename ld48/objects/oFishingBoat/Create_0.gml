/// @description 

// Inherit the parent event
event_inherited();
enum Upgrades {
	repair,
	engine,
	hull,
	oxygen,
	radar,
	light,
	harpoon,
	dynamite,
	count
}
descriptions = ["Repair","Engine Upgrade","Armour Upgrade","Oxygen Tank Upgrade","Radar Range Upgrade","Deep Sea Headlight", "Harpoon","Dynamite"];
price = [0,10,10,10,10,30,50,10];
purchases = array_create(Upgrades.count,0);
max_purchases = [999999999999,3,4,3,2,1,1,9999999999999];
sprites = [sRepair,sEngineIcon,sArmourUpgrade,sOxygenUpgrade,sRadarRange,sLightIcon,sHarpoonIcon,sDynamiteIcon];
cursor = 0;
hold_to_purchase = 0;