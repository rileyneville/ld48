/// @description 
price[Upgrades.repair] = ceil(oPlayer.max_hp-oPlayer.hp)*2;

var spacing = 64;
var width = (Upgrades.count-1)*spacing;
if (oPlayer.y < 20 && abs(oPlayer.x-x) < (width/2+spacing/2)) {
	var dx = x-width/2;
	var dy = y-116;
	last_cursor = cursor;
	cursor = clamp(round((oPlayer.x-dx)/spacing),0,Upgrades.count-1);
	for (var i = 0; i < Upgrades.count; i++;) {
		var c = c_white;
		if (purchases[i] >= max_purchases[i] || oPlayer.cash < price[i]) {
			c = c_gray;
		}
		draw_sprite_ext(sprites[i],0,dx,dy,1,1,0,c,1);
		if (i == cursor) {
			draw_sprite(sShopCursor,(hold_to_purchase/60)*6.5,dx,dy);
		}
		draw_set_color(c);
		if (purchases[i] < max_purchases[i]) {			
			draw_text(dx,dy+32,string(price[i]));
		}
		dx += 64;
	}
	
	draw_set_color(c_white);
	if (cursor != last_cursor) {
		hold_to_purchase = 0;
	}
	if (keyboard_check(ord("E")) && purchases[cursor] < max_purchases[cursor] && oPlayer.cash >= price[cursor]) {
		hold_to_purchase++;
	} else {
		hold_to_purchase = 0;
	}
	if (hold_to_purchase >= 60) {
		oPlayer.cash -= price[cursor];
		purchases[cursor]++;
		hold_to_purchase = 0;
		switch (cursor) {
				case Upgrades.repair: {
					oPlayer.hp = oPlayer.max_hp;
				} break;
				case Upgrades.engine: {
					oPlayer.move_force *= 1.5;
					price[cursor] *= 3;
				} break;
				case Upgrades.hull: {
					oPlayer.max_hp += 5;
					oPlayer.hp += 5;
					price[cursor] *= 2;
				} break;
				case Upgrades.oxygen: {
					oPlayer.max_oxygen *= 2;
					price[cursor] *= 2;
				} break;
				case Upgrades.radar: {
					oPlayer.radar_range += 350;
					price[cursor] *= 3;
				} break;
				case Upgrades.light: {
					oPlayer.has_light = true;
				} break;
				case Upgrades.harpoon: {
					oPlayer.has_harpoon = true;
				} break;
				case Upgrades.dynamite: {
					oPlayer.dynamite++;
				} break;
		}
	}
	var dx = x;
	var dy = y-132;
	if (purchases[cursor] >= max_purchases[cursor]) {
		draw_text(dx,dy,descriptions[cursor]+ " Sold Out");
	} else if (oPlayer.cash >= price[cursor]) {
		draw_text(dx,dy,"Hold E to Purchase " + descriptions[cursor]);
	} else {
		draw_text(dx,dy,"Cannot Afford " + descriptions[cursor]);
	}
}