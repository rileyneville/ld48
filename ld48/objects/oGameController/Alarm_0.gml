/// @description check spawning fish
alarm[0] = 30;

min_fish = 20;
if (instance_number(oFish) >= min_fish) {
	return;
}

enum FishSpawn {
	object,
	min_depth,
	max_depth,
	min_school_size,
	max_school_size
}

fish_spawns = [
	[oTinyFish,100,600,10,30],
	[oTinyFish,100,600,10,30],
	[oSmallFish,300,1500,5,20],
	[oSmallFish,300,1500,5,20],
	[oTurtle,500,2000,1,3],
	[oSmallShark,100,2000,1,1],
	[oSmallShark,1000,2000,1,5],
	[oTuna,1500,3000,1,3],
	[oSmallJelly,1500,5000,5,20],
	[oLargeShark,2500,5000,1,1]
];

var tries = 0;
do {
	tries++;
	var d = random_range(-90,90);
	if (irandom(1)) {
		d += 180;
	}
	var spawn_x = oPlayer.x+lengthdir_x(global.radar_range,d);
	var spawn_y = oPlayer.y+lengthdir_y(global.radar_range,d);
	var school_dir = d+180;

	var possible_fish = [];
	for (var i = 0; i < array_length(fish_spawns); i++;) {
		var f = fish_spawns[i];
		if (spawn_y < f[FishSpawn.max_depth] && spawn_y > f[FishSpawn.min_depth]) {
			possible_fish[array_length(possible_fish)] = f;
		}
	}
} until ((array_length(possible_fish) > 0) || (tries > 50));

if (array_length(possible_fish) > 0) {
	var f = possible_fish[irandom(array_length(possible_fish)-1)];
	school_dir += random_range(-30,30);
	var num_fish = random_range(f[FishSpawn.min_school_size],f[FishSpawn.max_school_size]);
	
	for (var i = 0; i < num_fish; i++;) {
		var d = random(360);
		var fish = instance_create_layer(spawn_x+lengthdir_x(i*8,d),spawn_y+lengthdir_y(i*8,d),"Fish",f[FishSpawn.object]);
		with (fish) {
			phy_speed_x = lengthdir_x(1,school_dir);
			phy_speed_y = lengthdir_y(1,school_dir);
		}
	}
}