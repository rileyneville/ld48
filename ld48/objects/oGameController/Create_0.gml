/// @description 
alarm[0] = 1;
camera = view_get_camera(0);
camera_w = camera_get_view_width(camera);
camera_h = camera_get_view_height(camera);
display_set_gui_size(camera_w,camera_h);
camera_x = oPlayer.x;
camera_y = oPlayer.y;
camera_set_view_pos(camera,camera_x,camera_y);

camera_top = camera_y - camera_h/2;
camera_bottom = camera_y + camera_h/2;

camera_left = camera_x - camera_w/2;
camera_right = camera_x + camera_w/2;

global.grav = 20;
global.radar_range = 1200;

global.ps_effects = part_system_create();
global.pt_bubbles = part_type_create();
part_type_sprite(global.pt_bubbles,sBubbles,true,true,false);
part_type_direction(global.pt_bubbles,0,360,0,5);
part_type_speed(global.pt_bubbles,0,1,-0.05,0);
part_type_orientation(global.pt_bubbles,0,360,0,0,0);
part_type_life(global.pt_bubbles,30,60);
part_type_alpha1(global.pt_bubbles,0.5);

global.pt_coins = part_type_create();
part_type_sprite(global.pt_coins,sCoin,true,true,false);
part_type_direction(global.pt_coins,60,120,0,0);
part_type_speed(global.pt_coins,3,5,-0.025,0);
part_type_orientation(global.pt_coins,0,360,0,0,0);
part_type_life(global.pt_coins,40,60);
part_type_gravity(global.pt_coins,0.1,270);

global.pt_blood = part_type_create();
part_type_sprite(global.pt_blood,sUnderwaterBlood,false,false,true);
part_type_direction(global.pt_blood,0,360,0,5);
part_type_size(global.pt_blood,0,0,0.01,0);
part_type_speed(global.pt_blood,0,1,-0.1,0);
part_type_orientation(global.pt_blood,0,360,0,0,0);
part_type_life(global.pt_blood,120,180);
part_type_alpha2(global.pt_blood,1,0);

surface_timer = 0;


function draw_water_surface(back) {
	var surface_res = 16;
	var surface_start = floor(camera_left/surface_res)*surface_res;
	surface_timer++;
	for (var sx = surface_start; sx <= camera_right+surface_res; sx+= surface_res) {
		var sy = dcos((sx+surface_timer)*2)*1 + dcos((sx-surface_timer)*2.3)*1;
		var player_influence = sqr(map_range(abs(sx-oPlayer.x),0,100,1,0,true)) *sqr(map_range(abs(oPlayer.y),0,64,1,0,true));
			sy = lerp(sy,oPlayer.y+oPlayer.phy_speed_y*2,player_influence);
			sy += dcos((abs(oPlayer.x-sx)+surface_timer)*4)*2*player_influence;
		if (sx != surface_start) {
			var sdir = point_direction(0,last_sy,surface_res,sy);
			draw_sprite_ext(sSurface,back,sx-surface_res,last_sy-4,1,1,sdir,c_white,1);
			//draw_line_width(sx-surface_res,last_sy,sx,sy,5);
		}
		var last_sy = sy;
	}
}
max_depth = 10000;
draw_set_font(f1);

