/// @description 
gpu_set_tex_filter(true);
draw_sprite_ext(sSky,camera_top > 0,camera_x,camera_y,camera_w/32,camera_h/32,0,c_white,1);
draw_sprite_ext(sDepthColours,0,camera_left,8,camera_w,max_depth/21,0,c_white,1);
gpu_set_tex_filter(false);

draw_water_surface(1);