/// @description 
if (surface_get_width(application_surface) != camera_w || surface_get_height(application_surface) != camera_h) {
	surface_resize(application_surface,camera_w,camera_h);	
}
camera_x = lerp(camera_x,oPlayer.x,0.05);
camera_y = lerp(camera_y,oPlayer.y,0.05);
camera_set_view_pos(camera,camera_x-(camera_w/2),camera_y-(camera_h/2));

camera_top = camera_y - camera_h/2;
camera_bottom = camera_y + camera_h/2;

camera_left = camera_x - camera_w/2;
camera_right = camera_x + camera_w/2;