/// @description 
var list = ds_list_create();
collision_circle_list(x,y,shock_range,oFish,true,true,list,false);
collision_circle_list(x,y,shock_range,oPlayer,true,true,list,false);
collision_circle_list(x,y,shock_range,oDynamite,true,true,list,false);
for (var i = 0; i < ds_list_size(list); i++;) {
	var o = list[|i];
	var dist = point_distance(x,y,o.x,o.y);
	var dir = point_direction(x,y,o.x,o.y);
	if (o.object_index != oDynamite) {
		var d = sqr(map_range(dist,0,damage_range,1,0,true))*damage;
		o.hp -= d;
		if (o.hp <= 0) {
			o.hp = 0;
		}
	}
	var f= sqr(map_range(dist,0,shock_range,1,0,true))*force;
	with (o) {
		o.phy_speed_x += lengthdir_x(f,dir);
		o.phy_speed_y += lengthdir_y(f,dir);
	}
}
ds_list_destroy(list);