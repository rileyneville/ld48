/// @description 
image_angle = 5*dcos(timer++);

if (abs(oPlayer.x-x) > 250) {
	spd = lerp(spd,clamp(oPlayer.x-x,-top_spd,top_spd),acc);
} else {
	spd = lerp(spd,0,acc);
}
x += spd;