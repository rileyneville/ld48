///@description ensures angle is between -179 and 180 degrees;
///@param angle
function normalise_angle(argument0) {
	var angle = argument0;
	//Make angle between 0 and 360
	angle = (angle + 360*100) mod 360;

	//Make angle between -179 and 180
	if (angle>180) {
		angle-=360;
	}
	return angle;


}
