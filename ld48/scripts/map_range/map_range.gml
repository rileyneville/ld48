function map_range() {
	//map_range(input,input_start,input_end,output_start,output_end)
	///@param input
	///@param input_start
	///@param input_end
	///@param output_start
	///@param output_end
	///@param [clamp]
	if (argument[0] == argument[1]) {
		return 	argument[3];
	}
	var val =  argument[3] + ((argument[4] - argument[3]) / (argument[2] - argument[1])) * (argument[0] - argument[1]);
	if (argument_count > 5) {
		if (argument[5]) {
			val = clamp(val,min(argument[3],argument[4]),max(argument[3],argument[4]));
		}	
	}
	return val;


}
