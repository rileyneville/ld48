/// @description given an x and y offset, returns the x offset based on image angle.
///@param xoffset
///@param yoffset
///@param [angle]
function yoff() {
	var xx = argument[0];
	var yy = argument[1];
	if (argument_count > 2) {
		var dd = argument[2];
	} else {
		dd = image_angle;
	}
	return lengthdir_y(xx,dd) + lengthdir_y(yy,dd-90);


}
