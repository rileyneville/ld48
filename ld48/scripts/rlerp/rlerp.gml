///@param val1
///@param val2
///@param amount
function rlerp(argument0, argument1, argument2) {
	var dir = argument0;
	var targetdir = argument1;
	var dirdiff = angle_difference(targetdir,dir);
	return ((dir+dirdiff*argument2) + 360) mod 360;


}
