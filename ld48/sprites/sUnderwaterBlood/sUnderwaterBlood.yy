{
  "bboxMode": 0,
  "collisionKind": 5,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 26,
  "bbox_top": 3,
  "bbox_bottom": 25,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"4e2d02d5-3d00-4de8-b541-62fcb59b251d","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"ac4d81f7-8072-4749-b8f7-cc9314fbd996","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"81c03ed5-d14c-494c-9571-26acd33432fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"02e94d39-c751-4470-b5ad-bf4b3ad0172d","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"bb5d4708-08de-468f-b2b6-fe63c70af057","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"e13bf233-1819-4b98-9819-240796ccc54a","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"24a2f43d-05cc-4d83-8ded-d7be9031c4c4","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"8706cfdb-63cc-472d-8fb0-8a94cb89fc17","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"bb5d4708-08de-468f-b2b6-fe63c70af057","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"02e94d39-c751-4470-b5ad-bf4b3ad0172d","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"e13bf233-1819-4b98-9819-240796ccc54a","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"24a2f43d-05cc-4d83-8ded-d7be9031c4c4","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"8706cfdb-63cc-472d-8fb0-8a94cb89fc17","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"18ea39ca-4433-4b83-a653-04d3e75840fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"bb5d4708-08de-468f-b2b6-fe63c70af057","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"02e94d39-c751-4470-b5ad-bf4b3ad0172d","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"e13bf233-1819-4b98-9819-240796ccc54a","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"24a2f43d-05cc-4d83-8ded-d7be9031c4c4","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"8706cfdb-63cc-472d-8fb0-8a94cb89fc17","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"fcebc065-4b5d-48f1-831f-5911b4655edb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"bb5d4708-08de-468f-b2b6-fe63c70af057","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"02e94d39-c751-4470-b5ad-bf4b3ad0172d","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"e13bf233-1819-4b98-9819-240796ccc54a","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"24a2f43d-05cc-4d83-8ded-d7be9031c4c4","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"8706cfdb-63cc-472d-8fb0-8a94cb89fc17","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"bb5d4708-08de-468f-b2b6-fe63c70af057","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"02e94d39-c751-4470-b5ad-bf4b3ad0172d","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"e13bf233-1819-4b98-9819-240796ccc54a","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"24a2f43d-05cc-4d83-8ded-d7be9031c4c4","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"LayerId":{"name":"8706cfdb-63cc-472d-8fb0-8a94cb89fc17","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"670ba41c-0707-4712-a403-216ee35919c5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"61113e35-60bb-4b4b-bd16-1a0902a20d27","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"41c9e916-7459-423a-a9a9-61eaadbbbcb5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18ea39ca-4433-4b83-a653-04d3e75840fa","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b5b6ac4-1fb4-4fe5-990b-9dd84c5b21c9","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fcebc065-4b5d-48f1-831f-5911b4655edb","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"38d5282d-4032-4eaa-af5f-f07e88449e7f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9aa5f0a-8a86-46e0-8149-3e2f2a6f4fdf","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"185f8b6e-0f58-42f6-b214-f32901d7a89f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c50ddfea-4d2e-44fe-b466-a4b3dc496e76","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sUnderwaterBlood","path":"sprites/sUnderwaterBlood/sUnderwaterBlood.yy",},
    "resourceVersion": "1.3",
    "name": "sUnderwaterBlood",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1 (2)","resourceVersion":"1.0","name":"8706cfdb-63cc-472d-8fb0-8a94cb89fc17","tags":[],"resourceType":"GMImageLayer",},
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default (2) (2) (2) (2)","resourceVersion":"1.0","name":"e13bf233-1819-4b98-9819-240796ccc54a","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1","resourceVersion":"1.0","name":"24a2f43d-05cc-4d83-8ded-d7be9031c4c4","tags":[],"resourceType":"GMImageLayer",},
    {"visible":false,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default (2) (2) (2)","resourceVersion":"1.0","name":"bb5d4708-08de-468f-b2b6-fe63c70af057","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ld48",
    "path": "ld48.yyp",
  },
  "resourceVersion": "1.0",
  "name": "sUnderwaterBlood",
  "tags": [],
  "resourceType": "GMSprite",
}