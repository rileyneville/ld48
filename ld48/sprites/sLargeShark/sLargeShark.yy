{
  "bboxMode": 2,
  "collisionKind": 5,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 11,
  "bbox_right": 52,
  "bbox_top": 10,
  "bbox_bottom": 24,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"dd1cb23d-ba37-43f6-aa89-31ce4f702eea","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd1cb23d-ba37-43f6-aa89-31ce4f702eea","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":{"name":"6c2c4c89-1e0a-491e-bfcc-2bbd95716a82","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sLargeShark","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"dd1cb23d-ba37-43f6-aa89-31ce4f702eea","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"28cbd10b-9d02-42cc-8aa6-a2584f610a17","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"28cbd10b-9d02-42cc-8aa6-a2584f610a17","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":{"name":"6c2c4c89-1e0a-491e-bfcc-2bbd95716a82","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sLargeShark","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"28cbd10b-9d02-42cc-8aa6-a2584f610a17","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc8a8942-9d1c-44d2-8920-2d2829af60be","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc8a8942-9d1c-44d2-8920-2d2829af60be","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":{"name":"6c2c4c89-1e0a-491e-bfcc-2bbd95716a82","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sLargeShark","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"cc8a8942-9d1c-44d2-8920-2d2829af60be","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"327b8997-f353-44d8-91f3-71195dface53","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"327b8997-f353-44d8-91f3-71195dface53","path":"sprites/sLargeShark/sLargeShark.yy",},"LayerId":{"name":"6c2c4c89-1e0a-491e-bfcc-2bbd95716a82","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sLargeShark","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","name":"327b8997-f353-44d8-91f3-71195dface53","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sLargeShark","path":"sprites/sLargeShark/sLargeShark.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 3.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"99e610c3-fa66-4ed0-bcd1-0e7d4549f2a7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd1cb23d-ba37-43f6-aa89-31ce4f702eea","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"21393c83-8241-49d6-98f0-af99abadefb6","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"28cbd10b-9d02-42cc-8aa6-a2584f610a17","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02fe979c-2f88-4d9a-9806-d10f3fef6fde","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc8a8942-9d1c-44d2-8920-2d2829af60be","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"31ed5beb-b291-4d67-afaf-575aa849e2e3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"327b8997-f353-44d8-91f3-71195dface53","path":"sprites/sLargeShark/sLargeShark.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 32,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sLargeShark","path":"sprites/sLargeShark/sLargeShark.yy",},
    "resourceVersion": "1.3",
    "name": "sLargeShark",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6c2c4c89-1e0a-491e-bfcc-2bbd95716a82","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Fish",
    "path": "folders/Fish.yy",
  },
  "resourceVersion": "1.0",
  "name": "sLargeShark",
  "tags": [],
  "resourceType": "GMSprite",
}