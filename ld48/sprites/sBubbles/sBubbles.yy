{
  "bboxMode": 0,
  "collisionKind": 5,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 13,
  "bbox_right": 18,
  "bbox_top": 13,
  "bbox_bottom": 18,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"da42cb01-6496-4590-9275-5ad9704cff94","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"da42cb01-6496-4590-9275-5ad9704cff94","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":{"name":"3646801f-8b07-4e0b-8ed5-ffe62febcb2d","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBubbles","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"da42cb01-6496-4590-9275-5ad9704cff94","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9fd506b4-511c-4240-b7f0-d7f8d0474b26","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9fd506b4-511c-4240-b7f0-d7f8d0474b26","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":{"name":"3646801f-8b07-4e0b-8ed5-ffe62febcb2d","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBubbles","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"9fd506b4-511c-4240-b7f0-d7f8d0474b26","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f11496c9-c4d4-44ae-b6dd-79d233f61a9c","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f11496c9-c4d4-44ae-b6dd-79d233f61a9c","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":{"name":"3646801f-8b07-4e0b-8ed5-ffe62febcb2d","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBubbles","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"f11496c9-c4d4-44ae-b6dd-79d233f61a9c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"40f26fd2-db80-473e-9195-58757eb1e773","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"40f26fd2-db80-473e-9195-58757eb1e773","path":"sprites/sBubbles/sBubbles.yy",},"LayerId":{"name":"3646801f-8b07-4e0b-8ed5-ffe62febcb2d","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sBubbles","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","name":"40f26fd2-db80-473e-9195-58757eb1e773","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sBubbles","path":"sprites/sBubbles/sBubbles.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4231f3a2-9733-48c9-9a9d-bde5ccf161e7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"da42cb01-6496-4590-9275-5ad9704cff94","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0c6637ce-0086-4819-a71c-5656b28c93fc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9fd506b4-511c-4240-b7f0-d7f8d0474b26","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7a140f3e-89a8-48de-a9b0-247ec6db1af0","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f11496c9-c4d4-44ae-b6dd-79d233f61a9c","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2d1a6bb6-793b-4326-8937-a79f221f07e8","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"40f26fd2-db80-473e-9195-58757eb1e773","path":"sprites/sBubbles/sBubbles.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 16,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sBubbles","path":"sprites/sBubbles/sBubbles.yy",},
    "resourceVersion": "1.3",
    "name": "sBubbles",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3646801f-8b07-4e0b-8ed5-ffe62febcb2d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ld48",
    "path": "ld48.yyp",
  },
  "resourceVersion": "1.0",
  "name": "sBubbles",
  "tags": [],
  "resourceType": "GMSprite",
}