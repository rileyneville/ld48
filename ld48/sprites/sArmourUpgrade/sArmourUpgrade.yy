{
  "bboxMode": 0,
  "collisionKind": 5,
  "type": 0,
  "origin": 1,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 13,
  "bbox_top": 1,
  "bbox_bottom": 14,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"84229249-79a3-4879-af05-41e47ad8ea6a","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"84229249-79a3-4879-af05-41e47ad8ea6a","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},"LayerId":{"name":"5c0eabbc-acf5-42ab-b376-367b1fc89ab2","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"sArmourUpgrade","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},"resourceVersion":"1.0","name":"84229249-79a3-4879-af05-41e47ad8ea6a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"sArmourUpgrade","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9541aad8-de9a-43a4-983d-3d62211bea0f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"84229249-79a3-4879-af05-41e47ad8ea6a","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"sArmourUpgrade","path":"sprites/sArmourUpgrade/sArmourUpgrade.yy",},
    "resourceVersion": "1.3",
    "name": "sArmourUpgrade",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5c0eabbc-acf5-42ab-b376-367b1fc89ab2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Player",
    "path": "folders/Player.yy",
  },
  "resourceVersion": "1.0",
  "name": "sArmourUpgrade",
  "tags": [],
  "resourceType": "GMSprite",
}